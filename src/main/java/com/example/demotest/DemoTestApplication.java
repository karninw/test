package com.example.demotest;

import com.example.demotest.entity.Employees;
import com.example.demotest.repository.EmployeesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class DemoTestApplication {

    @Autowired
    private EmployeesRepository employeesRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoTestApplication.class, args);
    }
}
