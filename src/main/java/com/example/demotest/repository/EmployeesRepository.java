package com.example.demotest.repository;

import com.example.demotest.entity.Employees;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeesRepository extends JpaRepository<Employees, Long> {

    public List<Employees> findByUsernameAndPassword(String username, String password);
}
