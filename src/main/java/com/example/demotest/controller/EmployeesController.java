package com.example.demotest.controller;

import com.example.demotest.entity.Employees;
import com.example.demotest.model.ResponseData;
import com.example.demotest.repository.EmployeesRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class EmployeesController {

    @Autowired
    EmployeesRepository employeesRepository;

    @GetMapping("/getToken")
    public String getToken() {
        return new Gson().toJson(new ResponseData("OK", "pass", "200"));
    }

    @GetMapping("/employees")
    public List<Employees> getEmployeesAll() {
        return employeesRepository.findAll();
    }

    @GetMapping("/employees/{id}")
    public Employees getEmployeesById(@PathVariable("id") long id){
        Optional<Employees> employeesOptional = employeesRepository.findById(id);
        return employeesOptional.get();
    }

    @PostMapping("/employeesSave")
    public String saveEmployees(@RequestBody Employees employees){
        String code ="";
        String msg = "";
        Employees employeesEntity = employeesRepository.save(employees);

        if(employeesEntity != null){
            code= "200";
            msg = "success";
        }else{
            code = "500";
            msg = "error";
        }

        return new Gson().toJson(new ResponseData("OK", msg, code));
    }

    @PutMapping("/employeesUpdate/{id}")
    public String updateEmployees(@RequestBody Employees employees, @PathVariable("id") long id){

        Optional<Employees> employeesOptional = employeesRepository.findById(id);

        if(!employeesOptional.isPresent()){
            return new Gson().toJson(new ResponseData("ERROR", "not found", "500"));
        } else{
            employees.setId(id);
            employeesRepository.save(employees);
        }

        return new Gson().toJson(new ResponseData("OK", "success", "200"));
    }

    @DeleteMapping("/employeesDelete/{id}")
    public void deleteEmployees(@PathVariable("id") long id){
        employeesRepository.deleteById(id);
    }




}
