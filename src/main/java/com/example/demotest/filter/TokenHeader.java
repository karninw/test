package com.example.demotest.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.demotest.model.EmployeesDto;
import com.example.demotest.model.ResponseData;
import com.example.demotest.repository.EmployeesRepository;
import com.example.demotest.util.Base64Util;
import com.google.gson.Gson;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

@Component
public class TokenHeader {
	private static Logger log = Logger.getLogger(TokenHeader.class);
	private static String  token = "accessToken";
	private static String  refreshToken = "refreshToken";
	private String secretKey;

	@Autowired
	Environment env;

	@Autowired
	EmployeesRepository employeesRepository;

	@PostConstruct
	protected void init() {
		log.info("======================= init TokenHeader ==========================");
		secretKey = Base64.getEncoder().encodeToString(env.getProperty("token.secretKey").getBytes());
	}
	

	public String getHeaderName(HttpServletRequest req, String headerName) {
		return req.getHeader(headerName);
	}
	
	public void setCookieOnly(HttpServletResponse response, String token ,String key){
		 Cookie cookie = new Cookie(key, token);
		 cookie.setHttpOnly(true);
		 cookie.setPath("/");
		 //cookie.setSecure(true);
		response.addCookie(cookie);
	}

	
	public void errorHeader(HttpServletResponse response , String exception) throws IOException {
		PrintWriter out = response.getWriter();
		out.print(httpMsgHeader(exception, HttpServletResponse.SC_METHOD_NOT_ALLOWED));
		out.flush();
		out.close();
	}

	public String httpMsgHeader(String msg, int httpCode){
		Gson gson = new Gson();
		return gson.toJson(new ResponseData("NOTOK", msg, httpCode));
	}
	
	public Map<String,String> getToken(EmployeesDto employeesDto){
		HashMap<String, String> map = new HashMap<>();
		try{
			Gson gson = new Gson();
			//set map token
			map.put(token, jwtEncodeAcessToken(Base64Util.encode(gson.toJson(employeesDto))));

		}catch (Exception e) {
			e.printStackTrace();
		}

		return map;
	}
	
	
	public String jwtDecode(String authToken) {
		String result = "";
		try {
			Claims ss = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken).getBody();
			result = Base64Util.decode(ss.get("iss").toString());
		} catch (SignatureException e) {
			log.info("==================== Signature not valid  ===================");
			e.printStackTrace();
		} catch (ExpiredJwtException e) {
			log.info("==================== token expired time out ===================");
			e.printStackTrace();
		} 
		return result;
	}
	
	public String jwtEncodeRefreshToken(String encodeStr){
		JwtBuilder jwt = null;
		System.out.println("=====================refresh token time Expiration >>>>> "+System.currentTimeMillis()+Integer.valueOf(env.getProperty("token.time.refresh")));
		System.out.println("===== Time >>>>> "+ new Date(System.currentTimeMillis()+Integer.valueOf(env.getProperty("token.time.refresh"))));
		try {
			jwt = Jwts.builder()
					.setIssuer(encodeStr)
					.setIssuedAt(new Date())
					.signWith(SignatureAlgorithm.HS256, env.getProperty("token.secretKey").getBytes("UTF-8"))
					.setExpiration(new Date(System.currentTimeMillis()+Integer.valueOf(env.getProperty("token.time.refresh"))));
		} catch (NumberFormatException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return jwt.compact();
	}
	
	public String jwtEncodeAcessToken(String encodeStr){
		JwtBuilder jwt = null;
		System.out.println("==================== token time Expiration >>>>> "+System.currentTimeMillis()+Integer.valueOf(env.getProperty("token.time")));
		System.out.println("===== Time >>>>> "+ new Date(System.currentTimeMillis()+Integer.valueOf(env.getProperty("token.time"))));
		try {
			jwt = Jwts.builder()
					.setIssuer(encodeStr)
					.setIssuedAt(new Date())
					.signWith(SignatureAlgorithm.HS256, env.getProperty("token.secretKey").getBytes("UTF-8"))
					.setExpiration(new Date(System.currentTimeMillis()+Integer.valueOf(env.getProperty("token.time"))));
		} catch (NumberFormatException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return jwt.compact();
	}

	public String validateToken(String authToken) {
		String result = "S";
		try {
			Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
		} catch (SignatureException e) {
			System.out.println("==================== Signature not valid  ===================");
			e.printStackTrace();
		} catch (ExpiredJwtException e) {
			System.out.println("==================== token expired time out ===================");
			result = "Expired";
		}
		return result;
	}

	public String createToken(String token, String key) {
		System.out.println("============ CreateToken ==============");
		System.out.println("============ Token : "+ token);
		System.out.println("============ key : "+ key);
		int time = 0;
		if(key.equals("T")){
			time = Integer.valueOf(env.getProperty("token.time"));
		} else if(key.equals("R")){
			time = Integer.valueOf(env.getProperty("token.time.refresh"));
		}
		JwtBuilder jwt = null;
		jwt = Jwts.builder().setIssuer(Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getIssuer()).setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, secretKey)
				.setExpiration(new Date(System.currentTimeMillis() + time));
		return jwt.compact();
	}
	
}
