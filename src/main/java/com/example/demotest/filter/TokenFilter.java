package com.example.demotest.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.demotest.model.EmployeesDto;
import com.example.demotest.repository.EmployeesRepository;
import com.example.demotest.util.Base64Util;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.GenericFilterBean;


@Configuration
public class TokenFilter extends GenericFilterBean {
	private static Logger log = Logger.getLogger(TokenFilter.class);
	@Autowired
	TokenHeader tokenHeader;

	@Autowired
	EmployeesRepository employeesRepository;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		log.info("======================================= doFilter ===============================================");
		String username = tokenHeader.getHeaderName((HttpServletRequest) request, "userName");
		String password = tokenHeader.getHeaderName((HttpServletRequest) request, "password");
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		Cookie[] cookies = httpServletRequest.getCookies();
		HashMap<String, String> setTokenMap = new HashMap<>();
		
		System.out.println("====================== username : "+username);
		System.out.println("====================== pass : "+password);
		try {
			if(!employeesRepository.findByUsernameAndPassword(username, password).isEmpty()) {
					Map<String, String> getToken = tokenHeader.getToken(new EmployeesDto(Base64Util.decodeAjexUtf8ToTis620(username), password));
					log.info("accessToken >>>>>> "+getToken.get("accessToken"));
					tokenHeader.setCookieOnly((HttpServletResponse) response, getToken.get("accessToken"), "accessToken");
			} else if(cookies != null){
				for (int i = 0; i < cookies.length; i++) {
					Cookie cookie = cookies[i];
					if(cookie.getValue().equals("error")){
						tokenHeader.errorHeader((HttpServletResponse) response, "not have cookies");
						System.out.println("======================================= null cookies ===============================================");
					}
					if(cookie.getName().equals("accessToken")){
						setTokenMap.put("accessToken", cookie.getValue());
					}
				}
				String valiAccessToken = tokenHeader.validateToken(setTokenMap.get("accessToken"));
				if (valiAccessToken.equals("S")) {
					tokenHeader.setCookieOnly((HttpServletResponse) response, tokenHeader.createToken(setTokenMap.get("accessToken"), "T"), "accessToken");
					tokenHeader.setCookieOnly((HttpServletResponse) response, setTokenMap.get("refreshToken"), "refreshToken");
				} else if(valiAccessToken.equals("Expired")) {
					tokenHeader.setCookieOnly((HttpServletResponse) response, tokenHeader.createToken(setTokenMap.get("refreshToken"), "T"), "accessToken");
					tokenHeader.setCookieOnly((HttpServletResponse) response, setTokenMap.get("refreshToken"), "refreshToken");
				} else if(valiAccessToken.equals("S")){
					tokenHeader.setCookieOnly((HttpServletResponse) response, tokenHeader.createToken(setTokenMap.get("accessToken"), "T"), "accessToken");
					tokenHeader.setCookieOnly((HttpServletResponse) response, tokenHeader.createToken(setTokenMap.get("accessToken"), "R") , "refreshToken");
				}else if(valiAccessToken.equals("Expired")){
					tokenHeader.errorHeader((HttpServletResponse) response, "accessToken and refreshToken Expired");
				}
			}else{
				tokenHeader.errorHeader((HttpServletResponse) response, "not have cookies");
				System.out.println("======================================= null cookies ===============================================");
			}

		} catch (Exception e) {
			tokenHeader.setCookieOnly((HttpServletResponse) response, "error", "username");
			tokenHeader.errorHeader((HttpServletResponse) response, e.toString());
			e.printStackTrace();
		}
		chain.doFilter(request, response);
	}

	private String chkNull(String value) {
		if(value == null){
			value = "";
		}
		return value;
	}
}
