package com.example.demotest.util;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Base64Util {
	public static String encode(String data){
		Base64.Encoder encoder = Base64.getEncoder();
		return encoder.encodeToString(data.getBytes(StandardCharsets.UTF_8));
	}
	
	public static String decode(String data){
		Base64.Decoder decoder = Base64.getDecoder();
		byte[] decodedByteArray = decoder.decode(data.toString());
		return new String(decodedByteArray, StandardCharsets.UTF_8);
	}
	
	
	 public static String decodeAjexUtf8ToTis620(String val) throws UnsupportedEncodingException, ScriptException {
		  String valDecode = "";
		  ScriptEngineManager factory = new ScriptEngineManager();
		  ScriptEngine engine = factory.getEngineByName("JavaScript");
		  val = "\"" + val + "\"";
		  valDecode = (String) engine.eval("var valDecode;" + "valDecode=decodeURIComponent(" + val + ");");
		  return valDecode;
	 }
}
