package com.example.demotest.model;

public class ResponseData {
	private String responseCode;
	private String responseMessage;
	private Object responseData;
	
	public ResponseData(){}
	
	
	public ResponseData(String responseCode, String responseMessage, Object responseDate){
		this.setResponseCode(responseCode);
		this.setResponseMessage(responseMessage);
		this.setResponseData(responseDate);
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}


	public Object getResponseData() {
		return responseData;
	}


	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}

}
